import numpy as np
import pandas as pd
from sklearn.tree import DecisionTreeClassifier

from sklearn.model_selection import train_test_split, KFold, cross_val_score
"""Decision tree classifier with SVD"""


# Load csv file into a dataframe
bcdf = pd.read_csv('CancerImagesSVDwithFeaturesSelected.csv')
feature_cols = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15']
X = bcdf[feature_cols]
y = bcdf['16']
# Training and testing sets
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.2,random_state=42)


# 5 folds selected
kfold = KFold(n_splits=5, random_state=1, shuffle=True)
# Call decision tree classifier using gini
clf_model = DecisionTreeClassifier(criterion="gini", random_state=42, max_depth=7, min_samples_leaf=8)

# Calculate cross validation score
results = cross_val_score(clf_model, X, y, cv=kfold)
print("Accuracy with SVD: %.3f%% (%.3f%%)" % (results.mean()*100.0, results.std()*100.0))


"""Decision Tree classifier with RFELR"""
# Load csv file into a dataframe

bcdf = pd.read_csv('CancerImagesSingularValuesRFELR2.csv')

#feature column names
feature_cols = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24']
#Declare features for dataset
X2 = bcdf[feature_cols]
# Declare class for  df
y2 = bcdf['25']
# Training and testing sets
X_train, X_test, y_train, y_test = train_test_split(X2, y2, test_size = 0.2,random_state=42)
# 5 folds selected
kfold = KFold(n_splits=5, random_state=1, shuffle=True)
# Call decision tree classifier using gini
clf_model = DecisionTreeClassifier(criterion="gini", random_state=42, max_depth=7, min_samples_leaf=8)

# Calculate cross validation score
results2 = cross_val_score(clf_model, X2, y2, cv=kfold)
print("Accuracy with LR: %.3f%% (%.3f%%)" % (results2.mean()*100.0, results2.std()*100.0))



"""clf_model.fit(X_train,y_train)

DecisionTreeClassifier(max_depth=7, min_samples_leaf=8, random_state=42)
# Predict on the model
y_predict = clf_model.predict(X_test)

print('Accuracy Score: ', accuracy_score(y_test,y_predict))

rmse = np.sqrt(mean_squared_error(y_test, y_predict))
print("RMSE: %f" % (rmse))"""