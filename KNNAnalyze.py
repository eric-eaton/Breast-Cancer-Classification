import pandas as pd
from matplotlib import pyplot as plt
from sklearn.model_selection import train_test_split, GridSearchCV
from sklearn.neighbors import KNeighborsClassifier
import numpy as np


# Load csv file into a dataframe
bcdf = pd.read_csv('CancerImagesSVDwithFeaturesSelected.csv')
feature_cols = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15']
X = bcdf[feature_cols]
y = bcdf['16']

print(y)
# Split into training and test set
X_train, X_test, y_train, y_test = train_test_split(
    X, y, test_size=0.2)
# first accuracy test
knn = KNeighborsClassifier(n_neighbors=4)
knn.fit(X_train, y_train)
# Calculate the accuracy of the model
print("Accuracy Score =", knn.score(X_test, y_test) * 100, "%")


#Test for highest accuracy
neighbors = np.arange(1, 10)
train_accuracy = np.empty(len(neighbors))
test_accuracy = np.empty(len(neighbors))
# Loop over K values
for i, k in enumerate(neighbors):
    knn = KNeighborsClassifier(n_neighbors=k)
    knn.fit(X_train, y_train)

    # Compute training and test data accuracy
    train_accuracy[i] = knn.score(X_train, y_train)
    test_accuracy[i] = knn.score(X_test, y_test)

# Generate plot
plt.plot(neighbors, test_accuracy, label='Testing dataset Accuracy')
plt.plot(neighbors, train_accuracy, label='Training dataset Accuracy')

plt.legend()
plt.xlabel('n_neighbors')
plt.ylabel('Accuracy')
plt.grid()
plt.show()

knn = KNeighborsClassifier(n_neighbors=4)
knn.fit(X_train, y_train)
# Calculate the accuracy of the model
print("Accuracy Score =", knn.score(X_test, y_test) * 100, "%")


