import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split, KFold, cross_val_score
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import LogisticRegression
from sklearn.pipeline import Pipeline
from sklearn import linear_model, decomposition, datasets

"""Logistic regression using SVD feature Selected dataset"""

# Load csv file into a dataframe
bcdf = pd.read_csv('CancerImagesSVDwithFeaturesSelected.csv')

#feature column names
feature_cols = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15']
#Declare features for unshuffled dataset
X = bcdf[feature_cols]
# Declare class for unshuffled df
y = bcdf['16']

pca = decomposition.PCA()




#Split train and test data (Unshuffled)
np.random.seed(42)
X_train, X_test, y_train, y_test = train_test_split(X, y)

"' Use Logistic regression model to predict accuracy"
scaler = StandardScaler()
# 5 folds selected
kfold = KFold(n_splits=5, random_state=1, shuffle=True)
# Call model with optimal hyperparameters
lr = LogisticRegression(C=1.2067926406393288, class_weight=None, dual=False, fit_intercept=True,
                   intercept_scaling=1, l1_ratio=None,
                   multi_class='auto', n_jobs=None, penalty='l2',
                   random_state=0, solver='liblinear', tol=0.0001, verbose=0,
                   warm_start=False)
"""model1 = Pipeline([('standardize', scaler),
                    ('log_reg', lr)])"""

# Calculate cross validation score
results = cross_val_score(lr, X, y, cv=kfold)
print("Accuracy with SVD: %.3f%% (%.3f%%)" % (results.mean()*100.0, results.std()*100.0))



"""y_train_hat = model1.predict(X_train)
y_train_hat_probs = model1.predict_proba(X_train)[:,1]

train_accuracy = accuracy_score(y_train, y_train_hat)*100
train_auc_roc = roc_auc_score(y_train, y_train_hat_probs)*100

print('Confusion matrix:\n', confusion_matrix(y_train, y_train_hat))

print('Training AUC: %.4f %%' % train_auc_roc)

print('Training accuracy: %.4f %%' % train_accuracy)

y_test_hat = model1.predict(X_test)
y_test_hat_probs = model1.predict_proba(X_test)[:,1]

test_accuracy = accuracy_score(y_test, y_test_hat)*100
test_auc_roc = roc_auc_score(y_test, y_test_hat_probs)*100

print('Confusion matrix:\n', confusion_matrix(y_test, y_test_hat))

print('Testing AUC: %.4f %%' % test_auc_roc)

print('Testing accuracy: %.4f %%' % test_accuracy)
# calculate RMSE
rmse = np.sqrt(mean_squared_error(y_test, y_test_hat))
print("RMSE: %f" % (rmse))


print(classification_report(y_test, y_test_hat, digits=6))"""


"""Logistic regression using RFELR feature selected dataset"""

# read csv into a dataframe
bcdf = pd.read_csv('CancerImagesSingularValuesRFELR2.csv')

#feature column names
feature_cols = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24']
#Declare features for dataset
X2 = bcdf[feature_cols]
# Declare class for  df
y2 = bcdf['25']


#Split train and test data (Unshuffled)
np.random.seed(42)
X_train, X_test, y_train, y_test = train_test_split(X2, y2)

"' Use Logistic regression model to predict accuracy"
scaler = StandardScaler()
# 5 folds selected
kfold = KFold(n_splits=5, random_state=1, shuffle=True)
# Call model with optimal hyperparameters
lr = LogisticRegression(C=1.2067926406393288, class_weight=None, dual=False, fit_intercept=True,
                   intercept_scaling=1, l1_ratio=None,
                   multi_class='auto', n_jobs=None, penalty='l2',
                   random_state=0, solver='liblinear', tol=0.0001, verbose=0,
                   warm_start=False)
"""model1 = Pipeline([('standardize', scaler),
                    ('log_reg', lr)])"""

# Calculate cross validation score
results = cross_val_score(lr, X2, y2, cv=kfold)
print("Accuracy with LR: %.3f%% (%.3f%%)" % (results.mean()*100.0, results.std()*100.0))

