import numpy as np
import pandas as pd
from sklearn.tree import DecisionTreeClassifier
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score,classification_report,confusion_matrix
from sklearn import tree
import graphviz

# Load csv file into a dataframe

bcdf = pd.read_csv('CancerImagesSingularValuesRFELR2.csv')

#feature column names
feature_cols = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24']
#Declare features for dataset
X = bcdf[feature_cols]
# Declare class for  df
y = bcdf['25']
# Training and testing sets
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.2,random_state=42)

# Call decision tree classifier using gini
clf_model = DecisionTreeClassifier(criterion="gini", random_state=42, max_depth=7, min_samples_leaf=8)
clf_model.fit(X_train,y_train)

DecisionTreeClassifier(max_depth=7, min_samples_leaf=8, random_state=42)
# Predict on the model
y_predict = clf_model.predict(X_test)

print('Accuracy Score: ', accuracy_score(y_test,y_predict))
