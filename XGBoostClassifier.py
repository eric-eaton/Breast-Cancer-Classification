from numpy import loadtxt
from sklearn.preprocessing import StandardScaler
from xgboost import XGBClassifier
from sklearn.model_selection import train_test_split, KFold, cross_val_score
from sklearn.metrics import accuracy_score
import pandas as pd
"""XGBoost Classifier with SVM"""
# Load csv file into a dataframe

bcdf = pd.read_csv('CancerImagesSVDwithFeaturesSelected.csv')

#feature column names
feature_cols = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15']
#Declare features for unshuffled dataset
X = bcdf[feature_cols]
# Declare class for unshuffled df
y = bcdf['16']

# split data into train and test sets
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=7)
scaler = StandardScaler()
# 5 folds selected
kfold = KFold(n_splits=5, random_state=1, shuffle=True)
# fit model on training data
model = XGBClassifier()
# Calculate cross validation score
results = cross_val_score(model, X, y, cv=kfold)
print("Accuracy with SVD: %.2f%% (%.2f%%)" % (results.mean()*100.0, results.std()*100.0))





"""XGBoost Classifier with RFELR """
# Load csv file into a dataframe

bcdf = pd.read_csv('CancerImagesSingularValuesRFELR2.csv')

#feature column names
feature_cols = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24']
#Declare features for unshuffled dataset
X2 = bcdf[feature_cols]
# Declare class for unshuffled df
y2 = bcdf['25']

# split data into train and test sets
X_train, X_test, y_train, y_test = train_test_split(X2, y2, test_size=0.2, random_state=7)
scaler = StandardScaler()
# 5 folds selected
kfold = KFold(n_splits=5, random_state=1, shuffle=True)
# fit model on training data
model = XGBClassifier()
# Calculate cross validation score
results2 = cross_val_score(model, X2, y2, cv=kfold)
print("Accuracy with LR: %.2f%% (%.2f%%)" % (results2.mean()*100.0, results2.std()*100.0))
""" without kfold cv: 
model.fit(X_train, y_train)
# make predictions for test data
y_pred = model.predict(X_test)
predictions = [round(value) for value in y_pred]
# evaluate predictions
accuracy = accuracy_score(y_test, predictions)
print("Accuracy: %.2f%%" % (accuracy * 100.0))

# note make ROC curve for each model for performance of all models
# False acceptance rate, false rejection rate - list
"""