import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.neural_network import MLPClassifier
from sklearn.metrics import confusion_matrix as cm
from sklearn.metrics import accuracy_score as acc
from sklearn.model_selection import KFold
from sklearn.model_selection import cross_val_score
from statistics import mean
from sklearn.preprocessing import StandardScaler


dataLR = pd.read_csv('CancerImagesSingularValuesRFELR2.csv')
dataSVM = pd.read_csv('CancerImagesSVDwithFeaturesSelected.csv')
cv = KFold(n_splits=5,random_state=1,shuffle=True)
X1 = np.array(dataSVM.iloc[:,0:16])
y1 = np.array(dataSVM['16'])

X2 = np.array(dataLR.iloc[:,0:25])
y2 = np.array(dataLR['25'])

sc = StandardScaler()
X1 = sc.fit_transform(X1)
X2 = sc.fit_transform(X2)
print("Running ---")
print()
ANN = MLPClassifier(max_iter=500,hidden_layer_sizes=(50,100,50),verbose=True)
scoresSVM = cross_val_score(estimator=ANN, X=X1, y =y1, cv =cv, n_jobs=-1,verbose=1)
scoresLR = cross_val_score(estimator=ANN, X=X2, y =y2, cv =cv, n_jobs=-1,verbose=1)

meanSVM = mean(scoresSVM)
meanLR = mean(scoresLR)
print("Mean Score SVM: {}".format(meanSVM))
print("Mean Score LR: {}".format(meanLR))






"""import matplotlib.pyplot as plt
plt.figure()
plt.xlabel("Iteration", size = 20)
plt.ylabel("Score(Accuracy)", size = 20)
plt.title("10-Fold Cross Validation Scores for ANN with SVM for RFE", size = 25)
plt.xticks(fontsize=15,ticks=np.arange(0,11))
plt.yticks(fontsize =15)
plt.plot(range(1, len(scores) + 1),
    scores,)
plt.show()"""

print()
