import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import confusion_matrix as cm
from sklearn.metrics import accuracy_score as acc
from sklearn.model_selection import KFold
from sklearn.model_selection import cross_val_score
from statistics import mean
from sklearn.preprocessing import StandardScaler

dataLR = pd.read_csv('CancerImagesSingularValuesRFELR2.csv')
dataSVM = pd.read_csv('CancerImagesSVDwithFeaturesSelected.csv')
#Cross Validation Function
cv = KFold(n_splits=5,random_state=1,shuffle=True)
#Assign Training and testing data
X1 = np.array(dataSVM.iloc[:,0:16])
y1 = np.array(dataSVM['16'])

X2 = np.array(dataLR.iloc[:,0:25])
y2 = np.array(dataLR['25'])

#Scale data
sc = StandardScaler()
X1 = sc.fit_transform(X1)
X2 = sc.fit_transform(X2)

#run KNN on svm dataset
KNNSVM = KNeighborsClassifier(n_neighbors=750, n_jobs=-1)
scores = cross_val_score(estimator=KNNSVM, X=X1, y =y1, cv =cv, n_jobs=-1,verbose=1)
mean_scoreSVM = mean(scores)

print("Mean Score on SVM Dataset: ", mean_scoreSVM)

#run KNN on LR dataset
KNNLR = KNeighborsClassifier(n_neighbors=750, n_jobs=-1)
scores = cross_val_score(estimator=KNNLR, X=X2, y =y2, cv =cv, n_jobs=-1,verbose=1)
mean_scoreLR = mean(scores)

print("Mean Score on LR Dataset: ", mean_scoreLR)
