import xgboost as xgb
from sklearn.metrics import mean_squared_error, r2_score
import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split, cross_val_score
from sklearn.metrics import accuracy_score
"""XGBoost regression with SVD"""
# Load csv file into a dataframe
bcdf = pd.read_csv('CancerImagesSVDwithFeaturesSelected.csv')
feature_cols = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15']
X = bcdf[feature_cols]
y = bcdf['16']
# Create a dmatrix data structure that XGBoost will support and give performance and efficiency gains
data_dmatrix = xgb.DMatrix(data=X,label=y)
# Create training and test sets
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=123)

# Instantiate XGBRegressor with adjusted hyperparameters
xg_reg = xgb.XGBRegressor(objective ='reg:linear', colsample_bytree = 0.3, learning_rate = 0.1,
                max_depth = 5, alpha = 10, n_estimators = 10)
# fit the model
xg_reg.fit(X_train,y_train)
# Use the model to make predictions
preds = xg_reg.predict(X_test)

rmse = np.sqrt(mean_squared_error(y_test, preds))
print("RMSE: %f" % (rmse))


# Test now using a 5 fold cross validation
params = {"objective":"reg:linear",'colsample_bytree': 0.3,'learning_rate': 0.1,
                'max_depth': 5, 'alpha': 10}
# Cv_results will contain test and train RMSE metrics
cv_results = xgb.cv(dtrain=data_dmatrix, params=params, nfold=5,
                    num_boost_round=50,early_stopping_rounds=10,metrics="rmse", as_pandas=True, seed=123)

# cv_results.head()
# Print rmse
print((cv_results["test-rmse-mean"]).tail(1))

kf_cv_scores = cross_val_score(xg_reg, X_train, y_train, cv=5 )
print("Accuracy Score SVD: %.2f" % kf_cv_scores.mean())

"""XGBoost Regression with RFELR"""

# Load csv file into a dataframe

bcdf = pd.read_csv('CancerImagesSingularValuesRFELR2.csv')

#feature column names
feature_cols = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24']
#Declare features for unshuffled dataset
X2 = bcdf[feature_cols]
# Declare class for unshuffled df
y2 = bcdf['25']
# Create a dmatrix data structure that XGBoost will support and give performance and efficiency gains
data_dmatrix = xgb.DMatrix(data=X,label=y)
# Create training and test sets
X_train, X_test, y_train, y_test = train_test_split(X2, y2, test_size=0.2, random_state=123)

# Instantiate XGBRegressor with adjusted hyperparameters
xg_reg = xgb.XGBRegressor(objective ='reg:linear', colsample_bytree = 0.3, learning_rate = 0.1,
                max_depth = 5, alpha = 10, n_estimators = 10)
# fit the model
xg_reg.fit(X_train,y_train)
# Use the model to make predictions
preds2 = xg_reg.predict(X_test)

rmse = np.sqrt(mean_squared_error(y_test, preds2))
print("RMSE: %f" % (rmse))


# Test now using a 10 fold cross validation
params = {"objective":"reg:linear",'colsample_bytree': 0.3,'learning_rate': 0.1,
                'max_depth': 5, 'alpha': 10}
# Cv_results will contain test and train RMSE metrics
cv_results2 = xgb.cv(dtrain=data_dmatrix, params=params, nfold=10,
                    num_boost_round=50,early_stopping_rounds=10,metrics="rmse", as_pandas=True, seed=123)

# cv_results.head()
# Print rmse
print((cv_results2["test-rmse-mean"]).tail(1))

kf_cv_scores2 = cross_val_score(xg_reg, X_train, y_train, cv=5)
print("Accuracy Score LR: %.2f" % kf_cv_scores2.mean())