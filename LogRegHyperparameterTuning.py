import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import LogisticRegression
from sklearn.pipeline import Pipeline
from sklearn.metrics import roc_curve, roc_auc_score, classification_report, accuracy_score, confusion_matrix, \
    plot_roc_curve
import matplotlib.pyplot as plt
from sklearn.utils import shuffle
from sklearn import linear_model, decomposition, datasets
from sklearn.model_selection import GridSearchCV


# Load csv file into a dataframe
from HyperParameterLR import std_slc, logistic_Reg

bcdf = pd.read_csv('CancerImagesSVDwithFeaturesSelected.csv')

#feature column names
feature_cols = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15']
#Declare features for unshuffled dataset
X = bcdf[feature_cols]
# Declare class for unshuffled df
y = bcdf['16']

pca = decomposition.PCA()


pipe = Pipeline(steps=[('std_slc', std_slc),
                           ('pca', pca),
                           ('logistic_Reg', logistic_Reg)])

n_components = list(range(1, X.shape[1] + 1, 1))

C = np.logspace(-4, 4, 50)
penalty = ['l1', 'l2']

parameters = dict(pca__n_components=n_components,
                      logistic_Reg__C=C,
                      logistic_Reg__penalty=penalty)
# Use gridSearch CV to find hyperparameters
clf_GS = GridSearchCV(pipe, parameters)
clf_GS.fit(X, y)

print('Best Penalty:', clf_GS.best_estimator_.get_params()['logistic_Reg__penalty'])
print('Best C:', clf_GS.best_estimator_.get_params()['logistic_Reg__C'])
print('Best Number Of Components:', clf_GS.best_estimator_.get_params()['pca__n_components'])
print();
print(clf_GS.best_estimator_.get_params()['logistic_Reg'])
