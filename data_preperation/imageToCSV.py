""""
This Module combines all the parts of this project and trims the unnecessary aspects.
This module will convert the image dataset into a csv file containing 30 features and a class
"""

import os
from imageio import imread
import numpy as np
import pandas as pd
from tqdm import tqdm
from sklearn.decomposition import PCA

class ImageToCSV:

    def __init__(self,dataset):
        self.dataset = dataset
        self.positiveCount, self.negativeCount, self.unusableCount, self.totalCount,\
        self.arrayImages,self.filenames, self.classList = self.image_to_array(self.dataset)
        self.entries = self.featureExtractor(self.arrayImages,self.classList,self.filenames,self.totalCount)
        self.dataFrame = pd.DataFrame(self.entries)
        self.dataFrame.to_csv('CancerImagesPCASingularValues.csv',index=False)


    def image_to_array(self,dataset):
        arrayImages = []
        classList = []
        names = []
        pc = 0
        nc = 0
        uc = 0
        count = 0
        print("\nConverting Images to an Array\n")
        for (rootDir, dirNames, filenames) in os.walk(dataset):
            for filename in filenames:
                if filename == "":
                    continue
                count = count + 1
        progressBar = tqdm(total=count)
        for (rootDir, dirNames, filenames) in os.walk(dataset):
            for filename in filenames:
                loadedImage = imread(os.path.join(rootDir,filename))
                if loadedImage.shape == (50,50,3):
                    arrayImages.append(loadedImage)
                    names.append(filename)
                    if filename[-5] == '0':
                        nc = nc+1
                        classList.append(0)
                    if filename[-5] == '1':
                        pc = pc+1
                        classList.append(1)
                else:
                    uc = uc +1
                progressBar.update(1)

        count = count - uc
        return pc, nc, uc, count, arrayImages,names,classList


    def featureExtractor(self, arrayImages,classList, filenames,count, in_features = 50):
        pca = PCA(n_components=in_features)
        index= 0
        entries = []
        progressBar = tqdm(total=count)
        print("\nExtracting Features\n")
        for array in arrayImages:
            condensedImage = self.condenser(array)
            pca.fit(condensedImage)
            features = pca.singular_values_
            features = np.append(features,classList[index])
            features = np.append(features,filenames[index])
            index = index + 1
            entries.append(features)
            progressBar.update(1)
        return entries

    def condenser(self, image):
        condensedImage = (image[:, :, 0] + image[:, :, 1] + image[:, :, 2])
        return condensedImage


data = ImageToCSV('dataset')